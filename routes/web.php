<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('anasayfa');
});

Route::get('Derspaylasim', function () {
    return view('dersnotlari');
});


Route::get('Projeler', function () {
    return view('Projeler');
});

 Route::resource('websayfam','WebController');

Route::resource('cevap','CevapController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
