<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cevap extends Model
{

    protected $fillable =
        [

            'id','name','soru','cevap','updated_at','created_at'

        ];
    protected $table="cevap";

}
