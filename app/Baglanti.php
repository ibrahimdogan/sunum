<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baglanti extends Model
{
    protected $fillable =
        [

            'name','email','phone','soru','updated_at','created_at'

        ];
    protected $table="iletisim";
}
