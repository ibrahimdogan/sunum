<?php

namespace App\Http\Controllers;

use App\Sorucevap;
use Illuminate\Http\Request;

class SorucevapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sorucevap  $sorucevap
     * @return \Illuminate\Http\Response
     */
    public function show(Sorucevap $sorucevap)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sorucevap  $sorucevap
     * @return \Illuminate\Http\Response
     */
    public function edit(Sorucevap $sorucevap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sorucevap  $sorucevap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sorucevap $sorucevap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sorucevap  $sorucevap
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sorucevap $sorucevap)
    {
        //
    }
}
