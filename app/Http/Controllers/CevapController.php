<?php

namespace App\Http\Controllers;
use App\Baglanti;
use App\cevap;
use Illuminate\Http\Request;

class CevapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cevap  $cevap
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cevap  $cevap
     * @return \Illuminate\Http\Response
     */
    public function edit(cevap $cevap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cevap  $cevap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guncele=Baglanti::find($id);
        $ekle=new cevap();
        $ekle->id=$id;
        $ekle->name=$guncele->name;
        $ekle->soru=$guncele->soru;
        $ekle->cevap=$request->cevap;
        $ekle->save();

    }


    public function destroy($id)
    {

    }
}
