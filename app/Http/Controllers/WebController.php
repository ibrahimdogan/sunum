<?php

namespace App\Http\Controllers;
use DB;
use App\Baglanti;
use App\cevap;
use Illuminate\Http\Request;
use Validator;
class WebController extends Controller
{

    public function index()
    {
        $gelen=DB::table('iletisim')->get();
        return view('dersnotlari.sorucevap',['sikayetler'=>$gelen]);
    }


    public function create()
    {

    }



    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'soru' => 'required',
            'phone' => 'numeric'

        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        $ekle=new Baglanti();
        $ekle->name=$request->name;
        $ekle->email=$request->email;
        $ekle->phone=$request->phone;
        $ekle->soru=$request->soru;
        $ekle->save();
       return redirect('websayfam');

    }


    public function show(Baglanti $baglanti)
    {

    }


    public function edit($id)
    {
        $guncelenecek=Baglanti::find($id);

        return view('dersnotlari.cevap',['cevaplanacak'=>$guncelenecek]);
    }

    public function update(Request $request, $id)
    {


    }


    public function destroy($id)
    {
        $cevap=DB::table('cevap')->where('id','=',$id)->get();
        return view('dersnotlari.cevaplanan',['cevaplar'=>$cevap]);

    }
}
