
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Use title if it's in the page YAML frontmatter -->
    <title>ibrahim doğan</title>

    <meta name="description" content="Instructions on how to install XAMPP for Windows distributions." />


    <link href="/dashboard/stylesheets/normalize.css" rel="stylesheet" type="text/css" /><link href="/dashboard/stylesheets/all.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <script src="/dashboard/javascripts/modernizr.js" type="text/javascript"></script>


    <link href="/dashboard/images/favicon.png" rel="icon" type="image/png" />


</head>

<body class="faq">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=277385395761685";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <h1><a href="/">İbrahim Doğan</a></h1>
            </li>

        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li class=""><a href="/">Anasayfa</a></li>
                <li class="active"><a href="/websayfam">Güncel Sorular</a></li>
                <li class=""><a target="_blank" href="#">DersNotları</a></li>

            </ul>
        </section>
    </nav>
</div>

<div id="wrapper">
    <div class="hero">
        <div class="row">
            <div class="large-12 columns">
                <h1> Laravel Sen Yap dersin o yapar </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-8 columns">
            <dl class="accordion">
                <dt>LARAVEL NEDİR ?</dt>
                <dd>
                    <p>Laravel ihtiyaç duyulan, gelişmiş bir çok özellik ve yapıyı üzerinde barındıran, </p>

                    PHP ve OOP tüm nimetlerinden yararlanan, web uygulamaları geliştirmeyi </p>
                    <p>sağlayan açık kaynak PHP framework’ tür.<br />
                        <b>“WEB SANATÇILARININ PHP FRAMEWORK’Ü” sloganıyla kendisini özetler ve hakkını verir.</b></p>
                </dd>
                <dt>Laravel Özellikleri?</dt>
               <dd>
                    <p>Yapısı gereği gerçekten çok sade ve temiz kod yazarak istediğiniz uygulamaları<br> geliştirme imkanı verir.</p>

                <p>Söz dizimi çok basit ve anlamlıdır. Alışmak için zorlanmazsınız, çabuk öğrenilebilir.</p>

                    <p>Bir kaç işlem barındıran küçük uygulamalardan, büyük kurumsal projelere kadar </p>
                    <p>her türlü web uygulamasını tasarlama esnekliğine sahiptir.</p><br><br>
<p>OOP ve PHP nin tüm nimetlerinden yararlanır böylece güncel php özelliklerinde oop uygun şekilde çalışırız.</p>
                </dd>
                <dt>Laravel Kurulum</dt>
                <dd>
                    <p>Windows üzerine Laravel kurmak için öncelikle Wamp Server veya Xampp’ ın bilgisayarınıza kurulu olması gerekiyor.  </p>
                      <p>Zaten php diliyle uğraşıyorsanız bu programların kullanımı hakkında bilgi sahibisinizdir.</p>
                    <p>Bir sonraki adım composer programının kurulması,<code> https://getcomposer.org/download/ </code> adresine</p>
                    <p>girdiğinizde aşağıdaki ekran görüntüsündeki linke tıklayarak composer setup’ını indirin.</p>
                    <p><img src="composer.png"></p>
                    <p><b>Composerin kurulumu tamamlandıysa şimdi gelelim laravelin kurulumuna.</b></p>
                    <p>1-cmd yazarak komut satırını açın.</p>
                    <p>2-cd C:\xampp\htdocs yazarak projeyi oluşturacağımız dizine geçiyoruz.<br> Wamp server kullananlarda benzer şekilde www dizinine geçiş yapması gerekiyor.</p>
              <p>3-komut satırına <code> composer create-project laravel/laravel projeismi --prefer-dist </code>yazarak laravel’in indirilmesini bekliyoruz. projeismi kısmını istediğiniz gibi değiştirebilirsiniz.</p>
<p><img src="kurulum.png"></p>
                </dd>
                      <dt>Laravel Dosya ve Dizin Yapısı</dt>
<dd>

    <p>Laravel ile ilgili bir önceki yazımda kurulumu gerçekleştirmiştik.</p>
    <p> Şimdi ise kurulumdan sonra, hazır şekilde gelen dosya ve dizin yapısını inceleyeceğiz. Aslında ilk başlarda dizin yapısı biraz karmaşık gibi görünse de bir süre sonra alışıyorsunuz.</p>
<p><h3>Laravel Dizin Yapısı</h3></p>
    <p><b>app dizini:</b> Bu dizin adından da tahmin edebileceğiniz üzere uygulamanızın çalışması için gerekli olan temel</p>
    <p> kodları içerir. Model ve Controller’ın hepsi bu dizin altında bulunur.</p>

    <p><b>bootstrap dizini:  </b>Bu dizinde otomatik yüklenmesi gereken önyükleme ve yapılandırma dosyaları bulunur.
            Ayrıca performans optimizasyonu sağlamak için gerekli cache önbellek dosyaları da burada saklanır.
            Bu dizinin tasarım için sıklıkla kullandığımız bootstrap ile ilgisi bulunmamaktadır.</p>

    <p><b>database dizini:</b> Veritabanı ile ilgili tüm işlemlerin saklandığı dizindir.
    Veritabanı tablolarınızı buradan oluşturabilir ve düzenleyebilir, tabloları otomatik olarak verilerle doldurabilirsiniz.
    Yani sürekli olarak uygulama geliştirirken mysql veya phpmyadmine giderek işlem yapmak zorunda değilsiniz.</p>

    <b></b>public dizini:</b> Ortak dizin, uygulamanıza giren tüm isteklerin giriş noktası olan index.php dosyasını içerir. Bu dizinde ayrıca Resim,
        JavaScript ve CSS gibi dosyalarınızı da tutabilirsiniz.</p>

    <p><b>resources dizini:</b> MVC deseninin View (Görünüm) ile ilgili dosyalarını barındırır. Ayrıca LESS,
        SASS veya JavaScript gibi derlenmemiş öğelerde burada tutulur.</p>

    <p><b>routes dizini:</b> MVC deseninde yönlendirme işlemleri veya rota (routing) için gerekli olan dosyaları içerir. Varsayılan olarak,
        Laravel ile birlikte üç rota dosyası bulunur: web.php, api.php ve console.php. web.api dosyasını sıklıkla kullanacağız.</p>


    <p><b>storage dizin:</b> Bu dizinde derlenmiş şekilde tutulan görünüm ( View), dosya tabanlı tutulan session ve cache verileri ile log dosyaları bulunur.</p>

    <p><b>vendor dizini:</b> Bu dizinde Composer’ın otomatik olarak indirdiği dependencies (bağımlılıklar) bulunur.</p>

    <p><b>.env dosyası:</b> Bu dosyada uygulamanın çalışabilmesi için gerekli uygulama, veritabanı ve mail server gibi ayarlar bulunmaktadır. Hızlı bir başlangıç yapmak için veritabanı bilgilerini girilmelidir.</p>
</dd>

                <dt>Laravel Rooting (Yönlendirme)</dt>
                <dd>
                    <p>Laravel Rooting (Yönlendirme)<br>

                        Rotalar, Laravel'de bir adres açıldığında o adresin döndüreceği sonuca ulaşması için belirlenen yollardır.İlk olarak Laraveli kurduğumuz dizindeki app/http/routes.php dosyasını açıyoruz.Rooting işlemlerimizi genel olarak bu dosyadan sağlayacağız.

                        Açılan dosyadaki var olan kodlara dokunmadan en alt satıra;<br>
                        <img src="route1.png">
                    </p>
                </dd>

                <dt>Laravel Veritabanı Bağlantısı</dt>
                <dd>
<p>
    Bu yazıda çok basit bir şekilde Laravel'de Model yapısını inceleyeceğiz. Ama daha önce Modelleri kullanmadan veritabanına nasıl
    müdahale edebiliyoruz buna bakalım. Bu yazıda Laravel'de Model konusuna geçmeden
    önce veritabanı bağlantısından ve işlemlerinden bahsedeceğiz. Laravel'de veritabanı ayarları
    <b>/app/config/database.php</b>
    dosyası içerisinde tanımlanmaktadır. Birden çok veritabanı için(mysql, sqlite, postgresql..) ayarlamalar yapılmıştır. Ama
    varsayılan olarak Mysql kullanılmaktadır. Eğer başka bir veritabanı kullanıcaksanız<b>default </b> anahtarının değerini
    değiştirmelisiniz. Biz burada mysql kullanacağız. Ve buna göre mysql anahtarını kendinize göre ayarlamalısınız.
</p>
                    <img src="database.png">
   Bağlanti adında bir model sınıfımız olsun veritabanımızda 1 tane tablomuz olsun 
                    tabloda şu stunlar yer alacaktır. name,email,phone,soru,updated_at,created_at
                    model sınıfımızın içeriği aşağıdaki gibi olacaktır.
                    <img src="database2.png">
                    veritabanı işlemlerini yapmadan önce  sayfanın üstüne <b>use DB </b> yazın DB import etmek için<br>
                    Veritabanındaki bir tablonun bütün verilerini getirmek

                    <code>$liste=DB::table('tablo_ismi')->get();</code><br>
istenilen herhangi bir veri getirmek için<br>
                    <code>$liste=Bağlantı::find(id);</code><br>
                    öncelikle model sınıfında bir nesne oluşturulur<br>
                    <b>Veri ekleme:</b><br>
                    <code>$ekle=new Bağlanti();<br>
                          $ekle->name="girilecek isim";<br>
                           $ekle->email="eklenecek email"<br>
                        $ekle->save();
                    </code>
                    <b>veri güncelleme</b><br>
                    öncelikle günceleyeceğimiz verini primer key ini almamız lazım<br>
                    <code>

                        $guncelle=Bağlanti::find(id);<br>
                        $guncelle->name="yeni isim";<br>
                        $guncelle->email="yeni email";<br>
                        $guncelle->sifre="yeni sifre";<br>
                        $guncelle->save();
                    </code>

                    <b>Veri Silme</b><br>
                    <code> $sil=Bağlanti::find(id);<br>
                           $sil->delete();
                    </code>
                </dd>

                </dd>
                </dd>
            </dl>
        </div>
    </div>

</div>

<footer>
    <div class="row">
        <div class="large-12 columns">
            <div class="row">
                <div class="large-8 columns">
                    <ul class="social">
                        <li class="twitter"><a href="#">Follow us on Twitter</a></li>
                        <li class="facebook"><a href="#">Like us on Facebook</a></li>
                        <li class="google"><a href="#">Add us to your G+ Circles</a></li>
                    </ul>

                    <ul class="inline-list">
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Privacy Policy</a></li>


                    </ul>
                </div>
                <div class="large-4 columns">
                    <p class="text-right">ibrahim doğan @ 2017</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- JS Libraries -->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/dashboard/javascripts/all.js" type="text/javascript"></script>
</body>
</html>
