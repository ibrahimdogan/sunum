<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Styles -->
    <script src="https://use.fontawesome.com/f572c0179b.js"></script>

</head>
@include('layouts.main');


<div class="content">

    <div class="container">


        <form class="form-horizontal" method="POST" action="/cevap{{isset($cevaplanacak) ? "/". $cevaplanacak->id:""}}">

            {{csrf_field()}}
            <fieldset>

                <!-- Form Name -->
                <legend>Form Name</legend>
            @if (isset($cevaplanacak))
                <input type="hidden" name="_method" value="PUT">
            @endif
            <div class="form-group">
                <label for="exampleInputEmail1">name</label>
                <input type="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$cevaplanacak->name}}">

            </div>




            <div class="form-group">
                <label for="exampleTextarea">soru:</label>
                <textarea class="form-control" id="exampleTextarea" rows="3" name="sour" placeholder="{{$cevaplanacak->soru}}"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleTextarea">yanıtla:</label>
                <textarea class="form-control" id="exampleTextarea" rows="3" name="cevap"></textarea>
            </div>


                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="yanıtla"></label>
                    <div class="col-md-4">
                        <input type="submit" id="yanıtla" name="yanıtla" class="btn btn-primary" value="Yanıtla">
                    </div>
                </div>

            </fieldset>




</div>
</div>
</body>
</html>
